using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    [SerializeField] private Slider slider;
    [SerializeField] private GameObject gameOverScreen; 

    private void Update()
    {
        if (slider.value <= 0)
        {
            Debug.Log("Game Over");
            gameOverScreen.SetActive(true);
            Time.timeScale = 0f;
        }
    }

    public void SetMaxHealth(int health)
    {
        slider.maxValue = health;
        slider.value = health; 
    }

    public void SetHealth(int health)
    {
        slider.value = health; 
    }
}
