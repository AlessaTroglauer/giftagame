using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Threshold : MonoBehaviour
{
    [SerializeField] private int maxHealth;
    [SerializeField] private int currentHealth;
    [SerializeField] private int damage;
    [SerializeField] private HealthBar healthBar; 

    private void Start()
    {
        currentHealth = maxHealth;
        healthBar.SetMaxHealth(maxHealth); 
    }

    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.tag == "Bar")
        {
            Debug.Log("In Range");
            StopAllCoroutines();
            Debug.Log("Stopping Coroutine"); 
        }
    }

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.tag == "Bar")
        {
            Debug.Log("Out of Range");
            StartCoroutine(LowerHealth(1f));
        }
    }

    private IEnumerator LowerHealth(float waitTime)
    {
        while (true)
        {
            yield return new WaitForSeconds(waitTime);
            TakeDamage(damage);
        }
    }

    private void TakeDamage(int damage)
    {
        currentHealth -= damage;

        healthBar.SetHealth(currentHealth);
    }
}
