using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThresholdMovement : MonoBehaviour
{
    private Rigidbody rb2d;
    [SerializeField] private float jumpForce = 1;
    [SerializeField] private float randomForce;
    [SerializeField] private float minTime;
    [SerializeField] private float maxTime;

    void Start()
    {
        rb2d = GetComponent<Rigidbody>();
        Invoke("AddRandomForce",0f);

    }

    void Update()
    {
        if (Input.GetButtonDown("Jump"))
        {
            rb2d.AddForce(new Vector2(0, jumpForce), ForceMode.Impulse); 
        }
    }

    private void AddRandomForce()
    {
         
        float randomTime = Random.Range(minTime, maxTime);
        rb2d.AddForce(new Vector3(0, randomForce), ForceMode.Impulse);
        Invoke("AddRandomForce", randomTime);
    }
}
