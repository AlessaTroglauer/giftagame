using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class StartMenu : MonoBehaviour
{

    [SerializeField] private string sceneOne;

    public void LoadLevel_01()
    {
        SceneManager.LoadScene(sceneOne);
    }


    public void QuitGame()
    {
        Application.Quit();
    }
}
