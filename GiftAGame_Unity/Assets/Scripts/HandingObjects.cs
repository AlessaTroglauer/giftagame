using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI; 

public class HandingObjects : MonoBehaviour
{
    //Variables to start task
    private float timePassed;
    [SerializeField] private float timeNeededOne;
    [SerializeField] private float timeNeededTwo;

    //Variables for time of task
    [SerializeField] private float currentTimerTime;
    [SerializeField] private float startingTime;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private TextMeshProUGUI timerTextTwo;

    //Variables for completing task
    [SerializeField] private bool isInRangeOne;
    [SerializeField] private bool isInRangeTwo;
    private bool questOneFinished;
    private bool questTwoFinished; 

    [SerializeField] private  GameObject image;
    [SerializeField] private Vector3 previousImagesize;
    [SerializeField] private Vector3 scaleChange;

    [SerializeField] private GameObject circleSawOne;
    [SerializeField] private GameObject circleSawTwo;
    [SerializeField] private GameObject circleSawSamara;


    [SerializeField] private Animator samaraAnimator; 

    private void Start()
    {
        currentTimerTime = startingTime;
        timerText.enabled = false;
        timerTextTwo.enabled = false;
        previousImagesize = image.transform.localScale;
        circleSawOne.SetActive(true);
        circleSawTwo.SetActive(false);
        circleSawSamara.SetActive(false);
    }

    private void Update()
    {
        timePassed += Time.deltaTime;

        ExecuteFirstTask();

        ExecuteSecondTask();

        /*if (currentTimerTime <= 0)
        {
            currentTimerTime = 5;
            timerText.enabled = false;

            if(questTwoFinished == true)
            {
                currentTimerTime = 0;
            }
        }
        */
    }

    private void ExecuteFirstTask()
    {
        if (timePassed >= timeNeededOne)
        {
            Debug.Log("Starting Task One");



            if (questOneFinished == false)
            {
                timerText.enabled = true;
                currentTimerTime -= 1 * Time.deltaTime;
                samaraAnimator.SetBool("Reaching", true);
                timerText.text = currentTimerTime.ToString("0");
            }




            if (currentTimerTime >= 0 && isInRangeOne == true && questOneFinished == false)
            {
                Debug.Log("Finished task one");

                //circleSawOne.SetActive(false);
                Destroy(circleSawOne);
                StartCoroutine(DisbaleSamarasSaw());

                samaraAnimator.SetBool("SawSwinging", true);
                samaraAnimator.SetBool("Reaching", false);

                currentTimerTime = startingTime;
                timerText.enabled = false;
                
                questOneFinished = true;
                //Destroy Object
                if (image.transform.localScale == previousImagesize)
                {
                    image.transform.localScale += scaleChange;
                    previousImagesize = image.transform.localScale;
                }
            }

            if (currentTimerTime <= 0 && questOneFinished == false)
            {
                Debug.Log("Failed Task one");

                circleSawOne.SetActive(false);

                samaraAnimator.SetBool("SawSwinging", false);
                samaraAnimator.SetBool("Reaching", false);

                currentTimerTime = startingTime;
                timerText.enabled = false;
                Destroy(circleSawOne);
                questOneFinished = true;
                //Destroy Object
                if (image.transform.localScale == previousImagesize)
                {
                    image.transform.localScale -= scaleChange;
                    previousImagesize = image.transform.localScale; 

                }
            }

        }
    }

    private void ExecuteSecondTask()
    {
        if (timePassed >= timeNeededTwo)
        {
            if (questTwoFinished == false)
            {
                circleSawTwo.SetActive(true);
                Debug.Log("Starting Task Two");
                timerTextTwo.enabled = true;
                currentTimerTime -= 1 * Time.deltaTime;
                timerTextTwo.text = currentTimerTime.ToString("0");
                samaraAnimator.SetBool("SawSwinging", false);
                samaraAnimator.SetBool("Reaching", true);

            }


            if (currentTimerTime >= 0 && isInRangeTwo == true && questTwoFinished == false)
            {
                Debug.Log("Finished Task Two");
                circleSawTwo.SetActive(false);
                StartCoroutine(DisbaleSamarasSaw());
                samaraAnimator.SetBool("SawSwinging", true);
                samaraAnimator.SetBool("Reaching", false);

                timerTextTwo.enabled = false;
                questTwoFinished = true;

                //Destroy Object
                if (image.transform.localScale == previousImagesize)
                {
                    Debug.Log("Changing Size"); 
                    image.transform.localScale += scaleChange;
                }
            }

            if (currentTimerTime <= 0 && questTwoFinished == false)
            {
                Debug.Log("Failed Task Two");
                circleSawTwo.SetActive(false);
                samaraAnimator.SetBool("Reaching", false);
                timerTextTwo.enabled = false;
                questTwoFinished = true;
                
                //Destroy Object
                if (image.transform.localScale == previousImagesize)
                {
                    image.transform.localScale -= scaleChange;

                }
            }
        }
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "ObjectToDragOne")
        {
            isInRangeOne = true;
        }

        if (other.tag == "ObjectToDragTwo")
        {
            isInRangeTwo = true;
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.tag == "ObjectToDragOne")
        {
            isInRangeOne = false;
        }

        if (other.tag == "ObjectToDragTwo")
        {
            isInRangeTwo = false;
        }
    }


    private IEnumerator DisbaleSamarasSaw()
    {
        circleSawSamara.SetActive(true);

        yield return new WaitForSeconds(6);

        circleSawSamara.SetActive(false);
    }
}
