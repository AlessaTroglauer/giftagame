using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro; 

public class Coutdown : MonoBehaviour
{
    [SerializeField] private float timeStart;
    [SerializeField] private TextMeshProUGUI textBox;
    [SerializeField] private GameObject gameWonScreen; 

    void Start()
    {

    }

    void Update()
    {
        if (timeStart >0 )
        {
            timeStart -= Time.deltaTime; 
        }
        else
        {
            timeStart = 0; 
        }
      
        DisplayTime(timeStart); 
    }

    void DisplayTime(float timeToDisplay)
    {
        if(timeToDisplay < 0)
        {
            timeToDisplay = 0;
            gameWonScreen.SetActive(true);
            Time.timeScale = 0f;
        }

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        textBox.text = string.Format("{0:00}:{1:00}", minutes, seconds); 
    }
}
